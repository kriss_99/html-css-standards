function w3_open() {
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("myOverlay").style.display = "block";
}

function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("myOverlay").style.display = "none";
}

function sendForm() {
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var message = document.getElementById("message").value;
  var formData = new FormData();

  formData.set("name", name);
  formData.set("email", email);
  formData.set("message", message);

  fetch("https://base/url/to/some/api", {
    body: formData,
    method: "post"
  });
}
